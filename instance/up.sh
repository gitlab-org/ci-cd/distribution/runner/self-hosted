#!/usr/bin/env bash

while getopts ":u:r:n:p:z:v:" opts; do
   case ${opts} in
      u) GITLAB_URL=${OPTARG} ;;
      r) REGISTRATION_TOKEN=${OPTARG} ;;
      n) RUNNER_NAME=${OPTARG} ;;
      p) PROJECT=${OPTARG} ;;
      z) ZONE=${OPTARG} ;;
      v) RUNNER_VERSION=${OPTARG} ;;
   esac
done

source initialize.sh

terraform init > /dev/null
terraform apply \
    -var="gitlab_url=$GITLAB_URL" \
    -var="registration_token=$REGISTRATION_TOKEN" \
    -var="runner_name=$RUNNER_NAME" \
    -var="runner_version=$RUNNER_VERSION" \
    -var="project=$PROJECT" \
    -var="zone=$ZONE"