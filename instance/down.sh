#!/usr/bin/env bash

while getopts ":p:z:v:" opts; do
   case ${opts} in
      p) PROJECT=${OPTARG} ;;
      z) ZONE=${OPTARG} ;;
      v) RUNNER_VERSION=${OPTARG} ;;
   esac
done

source initialize.sh

terraform init > /dev/null
terraform destroy \
    -var='gitlab_url=unused' \
    -var='registration_token=unused' \
    -var='runner_name=unused' \
    -var="runner_version=$RUNNER_VERSION" \
    -var="project=$PROJECT" \
    -var="zone=$ZONE"