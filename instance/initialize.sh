#!/usr/bin/env bash

if [[ -z $PROJECT ]]; then
    PROJECT=$(gcloud config get-value project 2>/dev/null)
    if [[ -z $PROJECT ]]; then
        read -p "Google Cloud project not set, please enter one now. You could also set the -p parameter of this script: " PROJECT
    else
        read -p "Using default project set from gcloud config '$PROJECT'. If you want to change the default zone you can use 'gcloud config set project my_project' or supply the -p parameter to this script (y/n)? " OK

        if [[ $OK != "y" ]]; then
            exit 1
        fi
    fi
fi

if [[ -z $ZONE ]]; then
    ZONE=$(gcloud config get-value compute/zone 2>/dev/null)
    if [[ -z $ZONE ]]; then
        read -p "Google Cloud zone not set, please enter one now. You could also set the -z parameter of this script: " ZONE
    else
        read -p "Using default zone set from gcloud config '$ZONE'. If you want to change the default zone you can use 'gcloud config set compute/zone europe-west1-c' or supply the -z parameter to this script (y/n)? " OK

        if [[ $OK != "y" ]]; then
            exit 1
        fi
    fi
fi

if [[ -z $RUNNER_VERSION ]]; then
    RUNNER_VERSION=latest
fi

if [[ -z $RUNNER_NAME ]]; then
    RUNNER_NAME=auto
fi