#!/bin/bash

set -e 
set -o pipefail

sudo apt update --fix-missing -y
sudo apt --fix-broken install -y
sudo apt install git -y

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash

# https://docs.gitlab.com/runner/install/linux-repository.html#apt-pinning
cat <<EOF | sudo tee /etc/apt/preferences.d/pin-gitlab-runner.pref
Explanation: Prefer GitLab provided packages over the Debian native ones
Package: gitlab-runner
Pin: origin packages.gitlab.com
Pin-Priority: 1001
EOF

apt-cache madison gitlab-runner
sudo apt-get install gitlab-runner=$RUNNER_VERSION