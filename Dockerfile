FROM gcr.io/google.com/cloudsdktool/cloud-sdk:308.0.0-alpine

RUN apk --update --no-cache add terraform

ADD . .

ENTRYPOINT ["./setup.sh"]