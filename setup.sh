#!/usr/bin/env bash

gcloud auth application-default print-access-token > /dev/null 2>&1
if [[ $? -ne 0 ]]; then
    gcloud auth application-default login || exit 1
fi

cd instance
case "$1" in 
    up)
        bash up.sh "${@:2}"
        ;;
    
    down)
        bash down.sh "${@:2}"
        ;;

    *)
        echo "only 'up' and 'down' are valid arguments"
        exit 1
        ;;
esac