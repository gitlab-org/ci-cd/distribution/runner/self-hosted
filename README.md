# Instructions

## Managing Runner VMs

### Creating Runner VMs with Docker

Set your specific [GitLab registration token](https://docs.gitlab.com/runner/register/#requirements) which will be used to register new runners, GCE project and zone in which the VMs will be created:

```shell
export YOUR_REGISTRATION_TOKEN=<YOUR_TOKEN>
export GCE_PROJECT_ID=<YOUR_GCE_PROJECT_ID>
export GCE_ZONE=<YOUR_GCE_ZONE>
```

Then run the setup script:

```shell
docker run --rm -it registry.gitlab.com/gitlab-org/ci-cd/distribution/runner/self-hosted \
    up \
    -u https://gitlab.com \
    -r $YOUR_REGISTRATION_TOKEN \
    -p $GCE_PROJECT_ID \
    -z $GCE_ZONE
```

This will prompt you to open a URL in your browser and then paste back the authentication code.

After that, follow the instructions.

### Creating Runner VMs without Docker

1. [Install Terraform CLI](https://learn.hashicorp.com/terraform/getting-started/install.html)
1. [Install gcloud CLI](https://cloud.google.com/sdk/install#installation_options)
1. Log into Google Cloud
   
   ```shell
   gcloud auth application-default login
   ```

1. Run

   ```shell
   ./setup.sh up -u https://gitlab.com -r YOUR_REGISTRATION_TOKEN
   ```

When new VMs come up the runners will show up in GitLab:

![image](resources/registered.jpg)

### Tearing down the VMs

Run

```shell
./setup.sh down
```

When the VMs are deleted or downscaled the registered runners will automatically deregister.

## Creating a base image

The base image has the GitLab Runner installed on it. From it VMs are then created.

Before starting, install [packer](https://www.packer.io/).

1. Log into the `gcloud` cli:

```shell
gcloud auth application-default login
```

2. Select the GCE project and zone in which you wish to create the new image. 
For example if creating a new official GitLab Auto Runner image:

    ```shell
    export GCE_PROJECT_ID=group-verify-df9383
    export GCE_ZONE=europe-west1-c
    ```

1. Pick the Runner version you wish preinstalled in the image along with the version displayed
in the image name:

    ```shell
    export RUNNER_VERSION=13.2.2
    export IMAGE_NAME_POSTFIX=13.2.2
    ```

    This will result in an image with the name `gitlab-auto-runner-13-2-2` with GitLab Runner `13.2.2` installed.

    If you wish to create the image `gitlab-auto-runner-latest` with GitLab Runner `13.2.2` installed:

    ```shell
    export RUNNER_VERSION=13.2.2
    export IMAGE_NAME_POSTFIX=latest
    ```

1. Build the image:

    ```shell
    cd image
    packer build packer.json
    ```

    In case the image already exists, run the build with `-force` to replace it:

    **Run with caution**
    ```shell
    packer build -force packer.json
    ```

1. Everything as a one-liner:

    ```shell
    GCE_PROJECT_ID=group-verify-df9383 \
    GCE_ZONE=europe-west1-c \
    RUNNER_VERSION=13.2.2 \
    IMAGE_NAME_POSTFIX=latest \
        packer build packer.json
    ```

1. If you wish to make the image accessible for all authenticated GCP users, regardless of their
organization, run:

    ```shell
    gcloud compute images add-iam-policy-binding gitlab-auto-runner-$IMAGE_NAME_POSTFIX \
        --member='allAuthenticatedUsers' \
        --role='roles/compute.imageUser'
    ```

    **Note**: To list all Gitlab Auto Runner images run:

    ```shell
    gcloud compute images list|grep gitlab-auto-runner
    ```

    **Note**: To delete an existing image run:

    ```shell
    gcloud compute images delete gitlab-auto-runner-$IMAGE_NAME_POSTFIX
    ```
